#!/usr/bin/env python3

''' Unittests for the atop logfile parser'''

import unittest
from aggregate import pre_parse

class AtopParserTest(unittest.TestCase):
    ''' Testing the atop parsing methods '''

    def test_pre_parse(self):
        result = pre_parse('./test-data/atop-testlog')
    
def main():
    unittest.main()

if __name__ == '__main__':
    main()
