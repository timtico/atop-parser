#!/usr/bin/env python3

''' Visualize aggregated system statistics'''

import aggregate
import matplotlib.pyplot as plt
import numpy as np

def visualize(atop_log_dir):
    parser = aggregate.AtopParser()
    stats = parser.get_stats_from_dir(atop_log_dir)
    x = np.array([stat.time_stamp for stat in stats])
    y = np.array([stat.memory_free for stat in stats])

    plt.plot(x,y)
    plt.show()


if __name__ == '__main__':
    visualize('/var/log/atop/')
