#!/usr/bin/env python3

''' Aggregate and parse the information from atop logfiles '''
import re
import subprocess
import sys
import collections
import datetime
import glob
import os

class AtopParser:
    ''' Parses atop compressed logfiles. Seeks the timestamp, CPU (%) and Memory (total, free)
    stats and aggregates these into named tuples. These named tuples can be used for further
    analysis, such as visualization '''

    def __init__(self):
        self.stats_pattern = re.compile("^ATOP.+?(\d{4}/\d\d/\d\d\s+?\d\d:\d\d:\d\d)+.+?^CPU.+?(\d+)\%.+?^MEM\s\|\s.+?(\d+\.\d[A-Z])+\s\|.+?(\d+\.\d[A-Z])",
                                        flags = re.MULTILINE | re.DOTALL)
        self.Stats = collections.namedtuple('Stats', ['time_stamp', 'cpu', 'memory_total', 'memory_free'])
        self._dt_format = "%Y/%m/%d  %H:%M:%S"
        self._symbols = dict(zip("B K M G T P".split(),[1,1e3,1e6,1e9,1e12,1e15]))

    def _make_timestamp(self, date_string):
        ''' convert string to datetime instance '''
        return datetime.datetime.strptime(date_string, self._dt_format)

    def _human_to_bytes(self, human_stat):
        ''' convert a human readable to bytes, for example 1M = 1000000.
        Probably the logfile can also contain bytes, in which case no symbols
        are used. In that case this is correctly processed and the 1 is used as multiplier'''
        return int(float(human_stat[0:-1]) * self._symbols.get(human_stat[-1], 1))

    def _read_atop(self, atop_logfile):
        ''' read the compressed atop logfile using the atop binary. This
        gives an ascii string as output '''
        p = subprocess.Popen(['/usr/bin/atop', '-r', atop_logfile],
                             stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        stdout, stderr = p.communicate()
        if stderr:
            sys.exit(stderr)
        else:
            return stdout.decode('ascii')

    def _parse(self, log_string):
        ''' uses regex to get the date, time, cpu usage, total and used memory
        from the given string. The string should be the output from the process call 
        atop -r /var/log/atop/<logfile> '''
        return re.findall(self.stats_pattern, log_string)

    def _convert_stats(self, stat):
        ''' convert the datestring to a datetime object, convert human readable integers
        for the memory stats to bytes '''
        stat_bytes = [self._human_to_bytes(h) for h in stat[2:]]
        return self.Stats(self._make_timestamp(stat[0]), int(stat[1]), *stat_bytes)

    def get_stats(self, atop_logfile):
        log_string = self._read_atop(atop_logfile)
        stats = self._parse(log_string)
        return [self._convert_stats(stat) for stat in stats]

    def _get_atop_files(self, directory):
        ''' gathers all atop logfiles from the given directory
        the filenames of this directory start with atop_ '''
        search_dir = os.path.join(directory, '/atop)*')
        if not os.path.exists(directory):
            sys.exit("Not a valid directory: {}".format(directory))
        return glob.glob(directory + "atop_*")

    def get_stats_from_dir(self, atop_logfile_directory):
        ''' parse all atop logfiles in a given directory, from which the filenames
        start with atop_ with atop_. Returns a list that is sorted by timestamp
        sorted flat list with named tuples containing the cpu and memory stats '''
        atop_files = self._get_atop_files(atop_logfile_directory) 
        # stats = [self.get_stats(file) for file in atop_files]
        stats = []
        for file in atop_files:
            stats.extend(self.get_stats(file))
        return sorted(stats, key = lambda t: t.time_stamp)

if __name__ == '__main__':
    test_dir = '/var/log/atop/'
    Parser = AtopParser()
    stats = Parser.get_stats('/home/t/pythoncode/atop-parse/test-data/atop_20180504')
    stats = Parser.get_stats_from_dir(test_dir)
    print(stats)


